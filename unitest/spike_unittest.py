import unittest
from rsd_unittest import RSD

Manual_Dictionary = {'6ilJq_05xRgek_8qUp36-g': {'User_Id1': 'WMTm9HHRA3EewoxTX1Gleg', 'User_Star3': '4', 'User_Star4': '4', 'User_Id0': '-V4cUmLyjk6Cv3hS0jw3hw', 'User_Review_ID4': 'DGGmV1P0vOGgp3kpRd6p-A', 'User_Review_ID0': 'AMNg45U8jRzwPENj5QmDSw', 'User_Review_ID2': 'MYrWcO00L2tk2g4iri_17Q', 'User_Review_ID3': 'BysFiyYjT0pe3zYS0zSFrQ', 'User_Review_ID1': 'CtxqLHFB80H64X62p_N0Bw', 'User_Star1': '4', 'User_Star2': '4', 'User_Review_ID5': 'SNmNLFCfIcclAMnpD5fKYA', 'User_Star0': '4', 'User_Id2': 'PwANTxC5mSWGVeSvU1zbYg', 'User_Id4': 'Q3fFv_ft17OyV-NRF1iQxw', 'User_Id5': 'q2ccFmRoK0lkkRPosgKFFg', 'User_Id3': '-OUKIatcSXuF-40IHcarwA', 'User_Star5': '4'}, '0hrB2iwQZ52VZcZWYywXrA': {'User_Id1': '5-4LQRi8LoGonuATXEE5TQ', 'User_Review_ID0': 'soh3WppI_zMdNmiIH_52Hg', 'User_Review_ID1': 'VWpgnzRyBzo8-ZXQyx0jzg', 'User_Star2': '1', 'User_Id0': '8kyyeRdryG9mbW0i7vCkgQ', 'User_Review_ID2': '4TXPBkkQkR3Oxc_Z0PmvjQ', 'User_Star0': '2', 'User_Id2': '5-4LQRi8LoGonuATXEE5TQ', 'User_Star1': '1'}, 'b9WZJp5L1RZr4F1nxclOoQ': {'User_Id1': 'Ei7ootM9QUDHeB1OJIkfIw', 'User_Star3': '1', 'User_Star4': '1', 'User_Id0': 'S5oBX7uhY4wYyDjvka4cWg', 'User_Review_ID4': 'eIZMWBIKI0JSbsUYT3_SFw', 'User_Review_ID0': 'SxaE8AKzEfYk-6SU3_LZhw', 'User_Review_ID2': 'TdGpzgCt97O82UVBzOn_FQ', 'User_Review_ID3': 'HvzazIqdwWcSVs0JFm9bcg', 'User_Review_ID1': 'm2CjXug2YN1_tDEPmAmogQ', 'User_Star1': '1', 'User_Star2': '1', 'User_Review_ID5': 'MzsMhW2KOydoyMXWXXKzWg', 'User_Star0': '2', 'User_Id2': 'Ft4NZwOB9xQJkRwuvsDO-A', 'User_Id4': 'AaXxndU7QoMwU9NptjMhSg', 'User_Id5': 'xr3YPIcYkGHZZ1lSnJpDKQ', 'User_Id3': 'yB0v8l4NMWJDeaq2h2mgUg', 'User_Star5': '1'}}

class Test(unittest.TestCase):
    def test_checking_RSD(self):
        self.assertEqual(RSD(50),Manual_Dictionary)

if __name__ == '__main__':
    unittest.main()