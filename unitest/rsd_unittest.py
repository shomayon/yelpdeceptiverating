#!/usr/bin/python3
#This module detect the spike among Ratings
__author__ = "Samira Zare" # - szare@ucsc.edu
__author__ = "Shide Homayon" # - shomayon@ucsc.edu

import csv
from collections import OrderedDict

def RSD(number_of_business):

    Count_business = 0  # control the number of business that we are checking the spike for
    f = open("C:\\Users\\User\\Documents\\Fall 2016\\Machine Learning\\project\\yelp_academic_dataset_review_.csv",
             encoding='latin-1')

    # read the first row from CSV file and put in reader as header of our columns
    reader = csv.reader(f)
    labels = next(reader)
    Spike_Dictionary = {}
    Business = str(0)
    Star_counter = 0
    initial = False

    # read Business ID from each row
    # If the Business ID is new, First calculate all the spike for the last Business ID, then
    # add the new one and clean the lists
    for main_row in reader:
        if Business != main_row[labels.index('business_id')]:
            if initial != False:    #If this is the First time you read the Business ID, do not calculate the last Business ID
                temperory_Star.sort()   #Sort the list of the Stars for one business in order to calculate their spike

            if Star_counter > 10:   #If there is more than 10 Reviews, calculate the spike since we need more Data to calculate the spike

                # Finding the three Quartiles by  finding their location and calculate them
                #Calculate the Spike range by using Quartiles
                if (Star_counter % 2 == 0):
                    x = int(Star_counter / 2)
                    Q2 = (float(temperory_Star[x - 1]) + float(temperory_Star[x])) / 2
                    if (x % 2 == 0):
                        y = int(x / 2)
                        Q1 = (float(temperory_Star[y]) + float(temperory_Star[y - 1])) / 2
                        Q3 = (float(temperory_Star[x + y - 1]) + float(temperory_Star[x + y])) / 2
                    else:
                        y = int((x - 1) / 2)
                        Q1 = float(temperory_Star[y])
                        Q3 = float(temperory_Star[x + y])
                else:
                    x = int((Star_counter - 1) / 2)
                    Q2 = float(temperory_Star[x])

                    if (x % 2 == 0):
                        y = int(x / 2)
                        Q1 = (float(temperory_Star[y]) + float(temperory_Star[y - 1])) / 2
                        Q3 = (float(temperory_Star[x + y]) + float(temperory_Star[x + y + 1])) / 2
                    else:
                        y = int((x - 1) / 2)
                        Q1 = float(temperory_Star[y])
                        Q3 = float(temperory_Star[x + y + 1])

                #specifying Quartile Ranges
                IQR = Q3 - Q1
                Outlier1 = Q3 + 1.5 * IQR
                Outlier2 = Q1 - 1.5 * IQR

                # making Index list in order to find which user ID and Review ID had the spike rate (Find their location)
                index_user = []
                i = 0

                # checking each user rating with the spike range for a business (to find the spiky ratings)
                for row in Star:
                    if int(row) > Outlier1:
                        flag_spiky_business = True  # we have a spike, add it's location to the index list
                        index_user.append(i)
                    elif int(row) < Outlier2:
                        flag_spiky_business = True  # we have a spike, add it's location to the index list
                        index_user.append(i)
                    i += 1

                spiky_business_list = []

                # If we have spike in that business, add it to the list of business with spike
                if flag_spiky_business == True:
                    spiky_business_list.append(Business)

                    spike_user = []

                    # add the information of the spiky ratings to the lists (User ID, Review ID and their ratings)
                    for row in index_user:
                        spike_user.append(user[row])
                        Spike_Star.append(Star[row])
                        Spike_Review_ID.append(Review_ID[row])

                    Spike_Dictionary[Business] = {}

                    # add each spiky business with the spiky users information to the Dictionary
                    # this is an output of our Review Spike Detection which will be the input of Feature Exctraction Module for
                    # the evaluation of spiky users
                    for row in range(len(index_user)):
                        Spike_Dictionary[Business]["User_Id" + str(row)] = spike_user[row]
                        Spike_Dictionary[Business]["User_Star" + str(row)] = Spike_Star[row]
                        Spike_Dictionary[Business]["User_Review_ID" + str(row)] = Spike_Review_ID[row]

                    print(Spike_Dictionary)

            #after checking and adding the last Business, now we add the new business information
            initial = True

            Business = main_row[labels.index('business_id')]
            print(Business)
            Count_business += 1 # to calculate number of business we checked until now
            if Count_business > number_of_business:
                return Spike_Dictionary

            # Clear the lists for the new business in order to calculate the new spikes
            Date = []
            Star = []
            temperory_Star = [] # we need a temperory list for Star so we can sort it later to calculate Quartiles
            user = []
            Review_ID = []
            Spike_Star = []
            Spike_Review_ID = []
            flag_spiky_business = False

            #add the information when we see the new business ID
            Star_counter = 0
            Review_ID.append(main_row[labels.index('review_id')])
            Date.append(main_row[labels.index('date')])
            Star.append(main_row[labels.index('stars')])
            temperory_Star.append(main_row[labels.index('stars')])
            Star_counter += 1   # in order to count how many reviews we have for one business to eliminate less than 10 reviews/ rating
            user.append(main_row[labels.index('user_id')])

            #add the information (User ID, Review ID, Date and Star)when we see the repeated business ID
        else:
            Review_ID.append(main_row[labels.index('review_id')])
            Date.append(main_row[labels.index('date')])
            Star.append(main_row[labels.index('stars')])
            temperory_Star.append(main_row[labels.index('stars')])
            Star_counter += 1   # in order to count how many reviews we have for one business to eliminate less than 10 reviews/ rating
            user.append(main_row[labels.index('user_id')])

    f.close()
