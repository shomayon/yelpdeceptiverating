# Location of all sample files for unit-test
SAMPLE_USER_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/sample_user.csv'
SAMPLE_REVIEW_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/sample_review.csv'
SAMPLE_POP_UID = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/YelpDeceptiveRating/sample_pop_uid.txt'

# Location of actual data files
USER_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/yelp_academic_dataset_user.csv'
REVIEW_FILE = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/Dataset/yelp_dataset_challenge_academic_dataset/CSV/yelp_academic_dataset_review.csv'
GEN_POP_UID = '/Users/viettrinh/Desktop/UCSC/CS_242/Project/YelpDeceptiveRating/gen_pop_uid.txt'
